const express = require("express");
const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const db = require('../services/database-server');
const UserAdmin = require('../models/UserAdminModel')
const config = require("../config.js");

const op = Sequelize.Op;
const router = new express.Router();

//
router.get('/:id', authenticationToken, (req, res) => {
    UserAdmin.findOne({
        where: {
            id: req.params.id
        }
    }).then(user_public => {
        res.json({ "data": user_public })
    })
})

router.get('/', authenticationToken, (req, res) => {
    UserAdmin.findAll({
        where: { id: { [op.not]: 1 } }
    }).then(user_public => {
        res.json({ "data": user_public })
    })
})

router.post('/', (req, res) => {
    var saltRounds = 10
    bcrypt.hash(req.body.password, saltRounds, (err, hash_password) => {
        var data = {
            nama: req.body.nama,
            email: req.body.email,
            password: hash_password
        }
        UserAdmin.create(data).then(user_admin => {
            if (user_admin) {
                res.json()
            } else {
                res.status(422).json({ "message": "Data tidak valid" })
            }
        }).catch((err) => {
            res.status(422).json({ "message": "Data tidak valid" })
        })
    })

})

router.patch('/', (req, res) => {

    UserAdmin.update({
        nama: req.body.nama,
        email: req.body.email
    }, {
        where: { id: req.body.id }
    }).then((affectedRow) => {
        if (affectedRow) {
            res.json()
        } else {
            res.status(422).json({ "message": "Data tidak valid" })
        }
    }).catch((err) => {
        res.status(422).json({ "message": "Data tidak valid" })
    })

})

router.patch('/password', (req, res) => {

    let id = req.body.id;
    let password = req.body.password;

    const saltRounds = 10;
    bcrypt.hash(password, saltRounds, (err, hash_password) => {
        UserAdmin.update({
            password: hash_password
        }, {
            where: { id: id }
        }).then((affectedRow) => {
            if (affectedRow) {
                res.json()
            } else {
                res.status(422).json({ "message": "Data tidak valid" })
            }
        }).catch((err) => {
            res.status(422).json({ "message": "Data tidak valid" })
        })

    })
})

router.delete('/', (req, res) => {
    UserAdmin.destroy({ where: { id: req.body.id } }).then(user_admin => {
        if (user_admin) {
            res.json()
        } else {
            res.status(422).json({ "message": "Data tidak valid" })
        }
    }).catch((err) => {
        res.status(422).json({ "message": "Data tidak valid" })
    })
})

module.exports = router

function authenticationToken(req, res, next) {
    const auth_header = req.headers['authorization'];
    const token = auth_header.split(' ')[1];
    // console.log(auth_header);
    // console.log(token);
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, config.access_token_secret, (err, email) => {
        if (err) return res.sendStatus(404);
        //req.email = email
        next()
    })
}