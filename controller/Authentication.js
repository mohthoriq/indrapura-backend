const express = require("express");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require("../config.js");

const UserAdmin = require('../models/UserAdminModel');

const router_api = new express.Router();

let refresh_token_storage = []

router_api.post('/login-admin', (req, res) => {

    var email = req.body.email
    var access_token = generateAccessToken(email);
    var refresh_token = jwt.sign(email, process.env.REFRESH_TOKEN_SECRET);

    UserAdmin.findOne({
        where: {
            email: email
        }
    }).then(user_result => {

        if (!user_result) {
            res.status(401).json({ message: "Pengguna belum terdaftar" });
        } else {
            let user = user_result.dataValues;
            let compare = new Promise((resolve, reject) => {
                bcrypt.compare(req.body.password, user.password).then(function (result) {
                    resolve(result)
                });
            });
            compare.then(result => {
                if (!result) {
                    res.status(401).json({ message: "Password Salah" });
                } else {
                    refresh_token_storage.push(refresh_token)
                    res.status(200).json({ message: "Login Berhasil", access_token: access_token, refresh_token: refresh_token });
                }
            }).catch((error) => {
                res.status(401).json({ message: "Password Salah" });
            });
        }

    }).catch(error => {
        res.status(404).json({ message: "Data tidak valid" });
    })

})

router_api.post('/new-token', (req, res) => {
    var refresh_token = req.body.refresh_token;
    if (refresh_token == null) return res.sendStatus(401);
    if (!refresh_token_storage.includes(refresh_token)) return res.sendStatus(403);

    jwt.verify(refresh_token, process.env.REFRESH_TOKEN_SECRET, (err, email) => {
        if (err) return res.sendStatus(403);
        var access_token = generateAccessToken(email);
        res.json({ access_token: access_token })
    })
})

module.exports = router_api

function generateAccessToken(email) {
    return jwt.sign({ email: email }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '2 days' });
}