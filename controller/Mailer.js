"use strict";
const express_hbs = require("express-handlebars");
const hbs = require("nodemailer-express-handlebars");
const path = require("path")
const nodemailer = require("nodemailer");

async function sendEmail(to, title, template, url_confirm) {
    let smtpTransporter = nodemailer.createTransport({
        host: "mail.indesc.co.id",
        port: 465,
        secure: true,
        auth: {
            user: "indescer@indesc.co.id",
            pass: "xVu3t*B?IO}g"
        }
    });

    var viewEngine = express_hbs.create({
        partialsDir: 'part/',
        defaultLayout: false
    });

    smtpTransporter.use('compile', hbs({
        viewEngine: viewEngine,
        viewPath: path.resolve(__dirname, '../views/page_auth'),
        extName: '.hbs'
    }));

    let mailInfo = await smtpTransporter.sendMail({
        from: "Tim IT GetSurvey (Ikatan Surveyor Indonesia) <indesc.team@indesc.co.id>",
        to: to,
        subject: title,
        template: template,
        context: {
            url_confirm: url_confirm
        }
    }, (err, info) => {
        console.log("Error: ", err, info);
    });
}

module.exports.sendEmail = sendEmail;