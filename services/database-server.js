const Sequelize = require('sequelize');

const config = require("../config.js");

let db_conn;

function connect() {

    if (!db_conn) {
        
        db_conn = new Sequelize(config.db_schema, config.db_username, config.db_password, {
            host: config.host,
            dialect: config.db_dialect,
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            }
        });

    }

    return db_conn;
}
module.exports.connect = connect;

function close() {
    db_conn.close();
}
module.exports.close = close;

