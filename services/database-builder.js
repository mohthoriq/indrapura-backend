
const bcrypt = require('bcrypt');

const UserAdmin = require('../models/UserAdminModel');

async function registerModel(db_conn) {
    console.log('--- Check Defined Model ---');
    let arr = [
        'user_admin'
    ];

    arr.forEach(obj => {
        console.log('Object [' + obj + ']' + (!db_conn.isDefined(obj) ? ' not ' : ' ') + 'defined');
    });

    // await db_conn.sync(
    //     { force: true }
    // ).then(function () {
    //     const saltRounds = 10;
    //     bcrypt.hash('import', saltRounds, (err, hash) => {
    //         // User Admin
    //         UserAdmin.bulkCreate([{
    //             id: 1,
    //             nama: 'Administrator',
    //             email: 'admin@kawan.tim',
    //             password: hash
    //         }, {
    //             id: 2,
    //             nama: 'Developer',
    //             email: 'dev@kawan.tim',
    //             password: hash
    //         }, {
    //             id: 3,
    //             nama: 'Maintainer 1',
    //             email: 'maintainer_1@kawan.tim',
    //             password: hash
    //         }, {
    //             id: 4,
    //             nama: 'Maintainer 2',
    //             email: 'maintainer_2@kawan.tim',
    //             password: hash
    //         }])
    //     });

    // })

}

module.exports.registerModel = registerModel;
