const http = require("http");
const express = require("express");
const compression = require('compression')

const path = require('path');
const cors = require("cors");
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const { ApolloServer, gql } = require('apollo-server-express');

const config = require("../config.js");
const router_auth = require("../controller/Authentication");
const router_user_admin = require("../controller/UserAdmin");

let httpServer;

async function initialize() {
  const app = express();

  app.use(compression())
  app.use(cors())
  app.use(bodyParser.json({ limit: '10mb' }));
  app.use(bodyParser.urlencoded({
    limit: '10mb'
  }));
  app.use(fileUpload({}));

  app.use('/public', express.static(path.join(__dirname, '../public'))); // for HTML resurces
  app.use('/files', express.static(path.join(__dirname, '../uploads'))); // for uploads path

  app.use('/api/auth/', router_auth);
  app.use('/api/users/admins', router_user_admin);

  // Construct a schema, using GraphQL schema language
  const typeDefs = gql`
    type Query {
      hello: String
    }
  `;

  // Provide resolver functions for your schema fields
  const resolvers = {
    Query: {
      hello: () => 'Hello world!',
    },
  };

  const server = new ApolloServer({ typeDefs, resolvers });
  server.applyMiddleware({ app });

  httpServer = http.createServer(app);
  httpServer
    .listen(config.web_port, config.web_host)
    .on("listening", () => {
      console.log(`Web server listening on ${config.web_host}:${config.web_port}`);
    }).on("error", err => {
      console.log(err);
    });

  app.get("/", (req, res) => {
    res.send('WELCOME TO API SERVER...');
  })


}

module.exports.initialize = initialize;

async function close() {
  return new Promise((resolve, reject) => {
    httpServer.close(err => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

module.exports.close = close;