
<header class="bg-white">
    <div class="container">
        <div class="navbar-up">
            <div class="left-menu">
                <a href="https://web.facebook.com/upz.duririau" class="fab fa-facebook-square"></a>
                <a href="https://www.youtube.com/channel/UCmecmMPEBXm96_Q37g8v-TQ" class="fab fa-youtube"></a>
                <a href="mailto:upz.ibad@yahoo.com" class="fas fa-envelope"></a>
                <a href="https://www.instagram.com/lazibadurrahman/" class="fab fa-instagram"></a>
                <a class="text">Ikuti Info Terbaru</a>
            </div>

            <div class="right-menu">
            <?php  
                $id = $this->session->userdata('_id');
                if (isset($id)) { 
                    $nama = $this->session->userdata('_nama');
                    $credential = $this->session->userdata('_credential');

                    if($credential=='bc460520e299cca847ced93ac41b3343' ){
                ?>
                    <div class="dropdown">
                        <div href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-user"></i> <?php echo $nama;?></div>
                        <div class="dropdown-menu dropdown-menu-right" style="right: 0; left: auto;">
                            <a class="dropdown-item" href="<?php echo site_url('UserDonatur/tampil/donasi_saya'); ?>">Halamanku</a>
                            <a class="dropdown-item" href="<?php echo site_url('Page_UD_Access/logout'); ?>">Log Out</a>
                        </div>
                    </div>
                    <?php } elseif($credential=='022e7d5408da83d2c9e7542c5de6a87d') {?>
                    <div class="dropdown">
                        <div href="#" class="dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-user"></i> <?php echo $nama;?></div>
                        <div class="dropdown-menu dropdown-menu-right" style="right: 0; left: auto;">
                            <a class="dropdown-item" href="<?php echo site_url('UserAdmin/tampil/ua_donasi_terbaru'); ?>">Donasi Terbaru</a>
                            <a class="dropdown-item" href="<?php echo site_url('Page_UA_Access/logout'); ?>">Log Out</a>
                        </div>
                    </div>
                    <?php }?>
                <?php }else{ ?>
                    <div class="guest-menu">
						<a href="<?php echo site_url('Page/tampil/form_registrasi'); ?>">Registrasi</a>
						<a href="<?php echo site_url('Page/tampil/form_login'); ?>">Login</a>
					</div>
                <?php } ?>
                <a class="ml-3 donasi-link" href=""><span class="fa fa-donate m-auto"></span> Donasi</a>
            </div>
        </div>
    </div>
    <div class="navbar-list"></div>
    <nav class="navbar navbar-expand-lg bg-white navbar-light">
        <div class="container">

            <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a href="<?php echo base_url()?>" class="navbar-brand order-first" href="index.html" style="text-decoration:none">
                <img class="logo" src="<?php echo base_url()?>src/img/logo.jpg" alt="">		
            </a>
            
            <div class="collapse navbar-collapse mr-3" id="collapse_target">
                <ul class="navbar-nav ml-auto">

                    <!-- Navigation -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('Page/tampil/artikel'); ?>">Artikel</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('Page/kalkulator_zakat'); ?>">Kalkulator Zakat</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Tentang Kami
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Sejarah</a>
                                <a class="dropdown-item" href="#">Visi Misi</a>
                                <a class="dropdown-item" href="#">Manajemen</a>
                                <a class="dropdown-item" href="#">Legal</a>
                                <a class="dropdown-item" href="#">Mitra</a>
                                <a class="dropdown-item" href="#">Karir</a>
                                <a class="dropdown-item" href="#">Kontak Kami</a>
                            </div>
                        </li>
                        
                        <li class="nav-item"><a class="nav-link" href="<?php echo site_url('Page/donasi/form_donasi_cek'); ?>">Cek Donasi</a></li>
                    </ul>
                </ul>
            </div>

            <!-- Login User -->
            <a href="<?php echo site_url('Page/donasi/form_donasi'); ?>" class="donasi-btn nav-link border rounded bg-main-color-1 text-white">
                <span class="fa fa-donate m-auto"></span> Donasi
            </a>

        </div>
    </nav>
    
</header>

<div class="thq-smoother"></div>