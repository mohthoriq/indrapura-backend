
<footer class=" pt-4 bg-light">
        <div class="container pb-2">
                <div class="row">
                        <div class="col-md-6">
                                <h6 class="text-uppercase mb-4 font-weight-bold">LAZ Ibadurrahman</h6>
                                <p>Mengelola dana ummat dan menyalurkannya kepada yang berhak merupakan amanah dari Allah yang berat yang kami pikul, bantu kami untuk meringankannya.</p>
                                <p class="text-right"><a class="text-main-color-1" href="#">Kenali Kami Lebih Dekat</a></p>
                        </div>
                        <div class="col-md-6">
                                <div class="row">
                                                <div class="col"><h6 class="text-uppercase font-weight-bold">Contact</h6></div> 
                                </div>
                                <div class="row">
                                                <div class="col-md-6">
                                                        <p><i class="fas fa-phone mr-3"></i> +62 765 92591</p>
                                                        <p><i class="fas fa-mobile-alt mr-3"></i> +62 823 91400366</p>
                                                </div>
                                                <div class="col-md-6">
                                                        <p><i class="fas fa-envelope mr-3"></i> admin@lazibadurrhman.com</p>
                                                        <p><i class="fas fa-home mr-3"></i> Jln. Mawar No 5 Duri - Riau #28884</p>
                                                </div>
                                </div>
                                <div class="row">
                                        <div class="col"><h6 class="text-uppercase font-weight-bold">Social</h6></div>
                                </div>
                                <div class="row">
                                        <ul class="list-unstyled list-inline" style="padding-left: 17px;">
                                                <li class="list-inline-item"><a href="https://web.facebook.com/upz.duririau" class="text-dark mx-1"><i class="fab fa-facebook-f"></i></a></li>
                                                <li class="list-inline-item"><a href="https://www.youtube.com/channel/UCmecmMPEBXm96_Q37g8v-TQ" class="text-dark mx-1"><i class="fab fa-youtube"></i></a></li>
                                                <li class="list-inline-item"><a href="https://www.instagram.com/lazibadurrahman/" class="text-dark mx-1"><i class="fab fa-instagram"></i></a></li>
                                        </ul>
                                </div>
                        </div>
                </div>
                                
        </div>
        <div class="text-white text-center py-3 text-sm bg-main-color-1" style="font-size:x-small">© 2018 Copyright:
                <a href="" class="text-white">MohThoriq</a>
        </div> 
</footer>
<a class="fa fa-chevron-up to-top" id="myBtn" href="#top" title="Go to top"></a>