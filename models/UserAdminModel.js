const Sequelize = require('sequelize');
const db = require('../services/database-server');

const UserAdminModel = {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nama: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        uniqe: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
}

module.exports = db.connect().define('user_admin', UserAdminModel);
