const webServer = require("./services/web-server.js");
const dbServer = require("./services/database-server.js");
const db_bulder = require("./services/database-builder.js");

async function startup() {
  console.log("Starting application...");

  try {
    console.log("Initializing web server module...");
    await webServer.initialize();
  } catch (err) {
    console.error(err);
    process.exit(1);
  }

  try {
    console.log("Initializing database module...");
    let db_conn = await dbServer.connect();
    if (db_conn) {
      console.log("Database Connected...");
      await db_bulder.registerModel(db_conn);
    } else {
      console.log("Database Connection Failed...");
    }

  } catch (err) {
    console.log("Database Connection Failed...");
    console.error(err);
    process.exit(1);
  }

}

startup();

async function shutdown(e) {
  let err = e;

  console.log("Shutting down");
  try {
    console.log("Closing database module");

    await database.close();
  } catch (err) {
    console.log("Encountered error", e);

    err = err || e;
  }

  try {
    console.log("Closing web server module");

    await webServer.close();
  } catch (e) {
    console.log("Encountered error", e);
  }

  console.log("Exiting process");

  if (err) {
    process.exit(1); // Non-zero failure code
  } else {
    process.exit(0);
  }
}

process.on("SIGTERM", () => {
  console.log("Received SIGTERM");

  shutdown();
});

process.on("SIGINT", () => {
  console.log("Received SIGINT");

  shutdown();
});

process.on("uncaughtException", err => {
  console.log("Uncaught exception");
  console.error(err);

  shutdown(err);
});
