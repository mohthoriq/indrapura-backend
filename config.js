require("dotenv").config();

module.exports = {
  web_host: "localhost",
  web_port: 5000,

  db_host: "localhost",
  db_port: 3306,
  db_dialect: "mysql",
  db_schema: "halalin",
  db_username: "root",
  db_password: "",

  //require('crypto').randomBytes(64).toString('hex')
  access_token_secret: "42bd755170c83747a9beaeff86d62e002d9f1512622b2096dd33a8263710e0e3311ce412dbee757f865afd1f511685084e1bc080074190f77c213b44207b1823",

  session_cookie_name:'coockie_app',
  session_cookie_secret:'12w98h998w7bfwe987j9',
};